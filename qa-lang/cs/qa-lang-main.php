<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	Description: Language phrases commonly used throughout Q2A


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php

	Translated automatically by the software 
		"https://github.com/ivomarvan/samples_and_experiments/machine_translation_question2answer"
		2022-01-31 20:16:07.744749
		To language: cs => Czech - Čeština
*/

return array(
	'_decimal_point' => '.',  // .
	'_thousands_separator' => ',',  // ,
	'_thousands_suffix' => 'k.',  // k
	'_millions_suffix' => 'M.',  // m
	'1_answer' => '1 reakce',  // 1 answer
	'1_comment' => '1 komentář',  // 1 comment
	'1_day' => '1 den',  // 1 day
	'1_disliked' => 'znelíbit',  // 1 dislike
	'1_flag' => '1 označení',  // 1 flag
	'1_hour' => '1 hodina',  // 1 hour
	'1_liked' => '1 oblíbit',  // 1 like
	'1_minute' => '1 minuta',  // 1 minute
	'1_month' => '1 měsíc',  // 1 month
	'1_point' => '1 Point.',  // 1 point
	'1_question' => '1 nápad',  // 1 question
	'1_second' => '1 sekunda',  // 1 second
	'1_tag' => '1 štítek',  // 1 tag
	'1_user' => '1 User.',  // 1 user
	'1_view' => '1 zobrazení',  // 1 view
	'1_vote' => '1 hlasování',  // 1 vote
	'1_week' => '1 týden',  // 1 week
	'1_year' => '1 rok',  // 1 year
	'add_category_x_favorites' => 'Přidat kategorii ^ do mých oblíbených',  // Add category ^ to my favorites
	'add_favorites' => 'Přidat do oblíbených',  // Add to my favorites
	'add_tag_x_favorites' => 'Přidat štítek ^ do mých oblíbených',  // Add tag ^ to my favorites
	'all_categories' => 'Všechny kategorie',  // All categories
	'anonymous' => 'anonymní',  // anonymous
	'answer_edited' => 'reakce upravena',  // answer edited
	'answer_reshown' => 'reakce znovu zobrazena.',  // answer reshown
	'answer_selected' => 'Vybraná reakce',  // answer selected
	'answered' => 'reagoval/a',  // answered
	'answered_qs_in_x' => 'Nápady s nejvíce reakcemi v ^',  // Most answered questions in ^
	'answered_qs_title' => 'Nápady s nejvíce reakcemi',  // Most answered questions
	'asked' => 'zeptal se',  // asked
	'asked_related_q' => 'dotaz k nápadu',  // asked related question
	'by_x' => 'podle ^',  // by ^
	'cancel_button' => 'zrušení',  // Cancel
	'closed' => 'Zavřeno',  // closed
	'comment_edited' => 'komentář upraven',  // comment edited
	'comment_moved' => 'komentář přesunu',  // comment moved
	'comment_reshown' => 'komentář znovu zobrazen.',  // comment reshown
	'commented' => 'komentovaný',  // commented
	'date_format_other_years' => '^Měsíc ^den, ^rok',  // ^month ^day, ^year
	'date_format_this_year' => '^Měsíc ^den',  // ^month ^day
	'date_month_1' => 'leden',  // Jan
	'date_month_2' => 'únor',  // Feb
	'date_month_3' => 'březen',  // Mar
	'date_month_4' => 'duben',  // Apr
	'date_month_5' => 'květen',  // May
	'date_month_6' => 'červen',  // Jun
	'date_month_7' => 'červenec',  // Jul
	'date_month_8' => 'srpen',  // Aug
	'date_month_9' => 'září',  // Sep
	'date_month_10' => 'říjen',  // Oct
	'date_month_11' => 'listopad',  // Nov
	'date_month_12' => 'prosinec',  // Dec
	'edited' => 'upravený',  // edited
	'email_error' => 'Při pokusu o odeslání e-mailu došlo k chybě.',  // An error occurred trying to send the email.
	'field_required' => 'Zadejte něco v tomto poli',  // Please enter something in this field
	'file_upload_limit_exceeded' => 'Velikost souboru překročí limity serveru',  // The size of the file exceeds the server\'s limits
	'general_error' => 'Došlo k chybě serveru - zkuste to znovu.',  // A server error occurred - please try again.
	'hidden' => 'skrytý',  // hidden
	'highest_users' => 'Nejlépe hodnocené uživatele',  // Top scoring users
	'hot_qs_in_x' => 'Žhavé nápady ^',  // Hot questions in ^
	'hot_qs_title' => 'Žhavé nápady',  // Hot questions
	'image_not_read' => 'Obraz nemohl být čten. Nahrajte jeden z: ^',  // The image could not be read. Please upload one of: ^
	'image_too_big_x_pc' => 'Tento obrázek je příliš velký. Prosím, měřítko to ^% pak zkuste to znovu.',  // This image is too big. Please scale to ^% then try again.
	'in_category_x' => 'v ^',  // in ^
	'ip_address_x' => 'IP adresa ^',  // IP address ^
	'logged_in_x' => 'Ahoj ^',  // Hello ^
	'max_length_x' => 'Maximální délka je ^ znaky',  // Maximum length is ^ characters
	'max_upload_size_x' => 'Maximální velikost nahrávání je ^',  // Maximum upload size is ^
	'me' => 'mě',  // me
	'meta_order' => '^co^když^kde^SZO',  // ^what^when^where^who	|	 you can reorder but DO NOT translate! e.g. <answered> <15 hours ago> <in Problems> <by me (500 points)>
	'min_length_x' => 'Uveďte více informací - alespoň ^ znaky',  // Please provide more information - at least ^ characters
	'moved' => 'přestěhovala',  // moved
	'nav_activity' => 'Veškerá činnost',  // All Activity
	'nav_admin' => 'Admin',  // Admin
	'nav_ask' => 'Vložit nápad',  // Ask a Question
	'nav_categories' => 'Kategorie',  // Categories
	'nav_feedback' => 'Poslat zpětnou vazbu',  // Send feedback
	'nav_home' => 'Domov',  // Home
	'nav_hot' => 'Žhavé!',  // Hot!
	'nav_login' => 'Přihlásit se',  // Login
	'nav_logout' => 'Odhlásit se',  // Logout
	'nav_most_answers' => 'S nejvíce reakcemi',  // Most answers
	'nav_most_recent' => 'Nedávné',  // Recent
	'nav_most_views' => 'Nejvíce zobrazení',  // Most views
	'nav_most_votes' => 'Nejvíce hlasů',  // Most votes
	'nav_no_answer' => 'Bez reakce',  // No answer
	'nav_no_selected_answer' => 'Bez vybrané reakce',  // No selected answer
	'nav_no_upvoted_answer' => 'Bez hlasované reakce',  // No upvoted answer
	'nav_qa' => 'Q & A.',  // Q&A
	'nav_qs' => 'Nápady',  // Questions
	'nav_register' => 'Registrovat',  // Register
	'nav_tags' => 'Štítky',  // Tags
	'nav_unanswered' => 'Bez reakce',  // Unanswered
	'nav_updates' => 'Moje aktualizace',  // My Updates
	'nav_users' => 'Uživatelé',  // Users
	'nav_info' => 'Nápověda',  
	'newest_users' => 'Nejnovější uživatele',  // Newest users
	'no_active_users' => 'Nebyli nalezeni žádné aktivní uživatele',  // No active users found
	'no_answers_found' => 'Nebyly nalezeny žádné reakce',  // No answers found
	'no_answers_in_x' => 'Žádné reakce ^',  // No answers in ^
	'no_categories_found' => 'No nalezeno žádné kategorie',  // No categories found
	'no_category' => 'Žádná kategorie',  // No category
	'no_comments_found' => 'Žádné komentáře nalezeny',  // No comments found
	'no_comments_in_x' => 'Žádné komentáře ^',  // No comments in ^
	'no_questions_found' => 'Nebyly nalezeny žádné nápady',  // No questions found
	'no_questions_in_x' => 'Žádné nápady ^',  // No questions in ^
	'no_related_qs_title' => 'Nebyly nalezeny žádné související nápady',  // No related questions found
	'no_results_for_x' => 'Nebyly nalezeny žádné výsledky ^',  // No results found for ^
	'no_tags_found' => 'Nalezeny žádné značky',  // No tags found
	'no_una_questions_found' => 'Nebyly nalezeny nápady bez reakcí',  // No unanswered questions found
	'no_una_questions_in_x' => 'Žádné nápady bez reakcí^',  // No unanswered questions in ^
	'no_unselected_qs_found' => 'Nebyly nalezeny žádné nápady bez vybrané reakce',  // No questions found without a selected answer
	'no_unupvoteda_qs_found' => 'Nebyly nalezeny žádné nápady bez souhlasné reakce',  // No questions found without an upvoted answer
	'page_label' => 'Strana:',  // Page:
	'page_next' => 'další',  // next
	'page_not_found' => 'Stránka nenalezena',  // Page not found
	'page_prev' => 'předchozí',  // prev
	'popular_tags' => 'Nejoblíbenější štítky',  // Most popular tags
	'questions_tagged_x' => 'Nedávné nápady označené ^',  // Recent questions tagged ^
	'recategorized' => 'rekreategorizace',  // recategorized
	'recent_activity_in_x' => 'Nedávná činnost ^',  // Recent activity in ^
	'recent_activity_title' => 'Poslední aktivita',  // Recent activity
	'recent_as_in_x' => 'Nápady, na které někdo nedávno reagoval: ^',  // Recently answered questions in ^
	'recent_as_title' => 'Nápady, na které někdo nedávno reagoval',  // Recently answered questions
	'recent_cs_in_x' => 'Nedávno komentované nápady v ^',  // Recently commented questions in ^
	'recent_cs_title' => 'Nedávno komentované nápady',  // Recently commented questions
	'recent_qs_as_in_x' => 'Nedávné nápady a reakce ^',  // Recent questions and answers in ^
	'recent_qs_as_title' => 'Nedávné nápady a reakce',  // Recent questions and answers
	'recent_qs_in_x' => 'Nedávné nápady ^',  // Recent questions in ^
	'recent_qs_title' => 'Nedávné nápady',  // Recent questions
	'related_qs_title' => 'Související nápady',  // Related questions
	'remove_favorites' => 'Odstranit z mých oblíbených položek',  // Remove from my favorites
	'remove_x_favorites' => 'Odstranit ^ z mých oblíbených',  // Remove ^ from my favorites
	'reopened' => 'znovuotevření',  // reopened
	'reshown' => 'reshown.',  // reshown
	'results_for_x' => 'Výsledky hledání pro ^',  // Search results for ^
	'retagged' => 'zpomalení',  // retagged
	'save_button' => 'Uložit změny',  // Save Changes
	'search_button' => 'Vyhledávání',  // Search
	'search_explanation' => 'Zadejte prosím text do vyhledávacího pole a zkuste to znovu.',  // Please enter some text into the search box and try again.
	'search_title' => 'Výsledky vyhledávání',  // Search results
	'selected' => 'vybraný',  // selected
	'send_button' => 'Poslat',  // Send
	'since_x' => 'od té doby ^',  // since ^
	'suggest_ask' => 'Vložte ^1nápad^2.',  // Help get things started by ^1asking a question^2.
	'suggest_category_qs' => 'Chcete-li zobrazit více, klikněte pro všechny ^1Nápady v této kategorii^2.',  // To see more, click for all the ^1questions in this category^2.
	'suggest_qs' => 'Chcete-li zobrazit více, klepněte na tlačítko ^1Úplný seznam nápadů^2.',  // To see more, click for the ^1full list of questions^2.
	'suggest_qs_tags' => 'Chcete-li zobrazit více, klepněte na tlačítko ^1Úplný seznam nápadů^2 nebo ^3populární štítky^4.',  // To see more, click for the ^1full list of questions^2 or ^3popular tags^4.
	'to_x' => 'na ^',  // to ^
	'unanswered_qs_in_x' => 'Nápady bez reakcí ^',  // Questions without answers in ^
	'unanswered_qs_title' => 'Nedávné nápady bez reakcí',  // Recent questions without answers
	'unselected_qs_in_x' => 'Nápady bez vybrané reakce ^',  // Questions without a selected answer in ^
	'unselected_qs_title' => 'Nedávné nápady bez vybrané reakce',  // Recent questions without a selected answer
	'unupvoteda_qs_in_x' => 'Nápady bez upřednostněné reakce ^',  // Questions without an upvoted answer in ^
	'unupvoteda_qs_title' => 'Nedávné nápady bez souhlasně hlasované reakce',  // Recent questions without an upvoted answer
	'upload_limit' => 'Příliš mnoho uploads - zkuste to znovu za hodinu',  // Too many uploads - please try again in an hour
	'view_q_must_be_approved' => 'Váš účet musí být schválen pro zobrazení nápadů. Čekejte prosím OR. ^1Přidat další informace^2.',  // Your account must be approved to view question pages. Please wait or ^1add more information^2.
	'view_q_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 Zobrazení nápadů.',  // Please ^5confirm your email address^6 to view question pages.
	'view_q_must_login' => 'Prosím ^1přihlásit se^2 nebo ^3Registrovat^4 Zobrazení nápadů.',  // Please ^1log in^2 or ^3register^4 to view question pages.
	'viewed_qs_in_x' => 'Nejsledovanější nápady v ^',  // Most viewed questions in ^
	'viewed_qs_title' => 'Nejsledovanější nápady',  // Most viewed questions
	'vote_disabled_approve' => 'Váš účet musí být schválen dříve, než budete moci hlasovat',  // Your account must be approved before you can vote
	'vote_disabled_down' => 'Hlasování dolů je k dispozici pouze některým uživatelům',  // Voting down is only available to some users
	'vote_disabled_down_approve' => 'Váš účet musí být schválen dříve, než budete moci hlasovat dolů',  // Your account must be approved before you can vote down
	'vote_disabled_hidden_post' => 'Nemůžete hlasovat o skrytých příspěvcích',  // You cannot vote on hidden posts
	'vote_disabled_hidden_a' => 'Nemůžete hlasovat o skrytých reakcích',  // You cannot vote on hidden answers	|	 @deprecated
	'vote_disabled_hidden_q' => 'Nemůžete hlasovat o skrytých nápadech',  // You cannot vote on hidden questions	|	 @deprecated
	'vote_disabled_level' => 'Hlasování je k dispozici pouze některým uživatelům',  // Voting is only available to some users
	'vote_disabled_my_post' => 'Nemůžete hlasovat o svých vlastních příspěvcích',  // You cannot vote on your own posts
	'vote_disabled_my_a' => 'Nemůžete hlasovat o svých vlastních reakcích',  // You cannot vote on your own answers	|	 @deprecated
	'vote_disabled_my_q' => 'Nemůžete hlasovat o svých vlastních nápadech',  // You cannot vote on your own questions	|	 @deprecated
	'vote_disabled_q_page_only' => 'Zobrazit tento nápad hlasovat',  // Please view this question to vote
	'vote_disabled_queued' => 'Můžete hlasovat pouze o schválených příspěvcích',  // You can only vote on approved posts
	'vote_down_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 hlasovat dolů.',  // Please ^5confirm your email address^6 to vote down.
	'vote_down_popup' => 'Kliknutím hlasujete dolů',  // Click to vote down
	'vote_limit' => 'Příliš mnoho obdržených hlasů - Zkuste to prosím znovu za hodinu',  // Too many votes received - please try again in an hour
	'vote_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 volit.',  // Please ^5confirm your email address^6 to vote.
	'vote_must_login' => 'Prosím ^1přihlásit se^2 nebo ^3Registrovat^4 volit.',  // Please ^1log in^2 or ^3register^4 to vote.
	'vote_not_allowed' => 'Hlasování na to není povoleno',  // Voting on this is not allowed
	'vote_up_popup' => 'Klikněte pro hlasování nahoru',  // Click to vote up
	'voted_down_popup' => 'Hlasovali jste to dolů - klikněte pro odstranění hlasování',  // You have voted this down - click to remove vote
	'voted_qs_in_x' => 'Nápady s nejvyšším skóre v hlasování ^',  // Highest voted questions in ^
	'voted_qs_title' => 'Nejvyšší hlasování',  // Highest voted questions
	'voted_up_popup' => 'Hlasovali jste to nahoru - kliknutím odstraníte hlasování',  // You have voted this up - click to remove vote
	'written' => '',  // 	|	 blank in English - placeholder for other languages
	'x_ago' => '^ před',  // ^ ago
	'x_answers' => '^ reakce',  // ^ answers
	'x_comments' => '^ komentáře',  // ^ comments
	'x_days' => '^ dnů',  // ^ days
	'x_disliked' => '^ nemít rád',  // ^ dislike
	'x_flags' => '^ označení',  // ^ flags
	'x_hours' => '^ hodiny',  // ^ hours
	'x_liked' => '^ jako',  // ^ like
	'x_minutes' => '^ minut',  // ^ minutes
	'x_months' => '^ měsíce',  // ^ months
	'x_points' => '^ body',  // ^ points
	'x_questions' => '^ Nápady',  // ^ questions
	'x_seconds' => '^ sekundy',  // ^ seconds
	'x_tags' => '^ štítky',  // ^ tags
	'x_users' => '^ Uživatelé',  // ^ users
	'x_views' => '^ pohledy',  // ^ views
	'x_votes' => '^ hlasování',  // ^ votes
	'x_weeks' => '^ týdny',  // ^ weeks
	'x_years' => '^ roky',  // ^ years
);
