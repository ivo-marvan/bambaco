<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	Description: Language phrases for question page


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php

	Translated automatically by the software 
		"https://github.com/ivomarvan/samples_and_experiments/machine_translation_question2answer"
		2022-01-31 20:34:45.503052
		To language: cs => Czech - Čeština
*/

return array(
	'1_answer_title' => '1 reakce',  // 1 Answer
	'a_convert_to_c' => 'Převést tuto reakci na komentář',  // Convert this answer into a comment
	'a_convert_to_c_on' => 'Převést tuto reakci na komentář k:',  // Convert this answer into a comment on:
	'a_convert_warn' => 'VAROVÁNÍ: Tato konverze nemůže být obrácena.',  // Warning: This conversion cannot be reversed.
	'a_convert_warn_cs' => 'VAROVÁNÍ: Tato změna nemůže být navrácena a přesune také komentáře k reakci.',  // Warning: This conversion cannot be reversed and will also move this answer\'s comments.
	'a_notify_email' => 'Napište mi na této adrese, pokud je moje reakce vybrána nebo komentována:',  // Email me at this address if my answer is selected or commented on:
	'a_notify_label' => 'Napište mi, zda je moje reakce vybrána nebo komentována',  // Email me if my answer is selected or commented on
	'a_notify_x_label' => 'Napiš mi email (^) Pokud je moje reakce vybrána nebo komentována',  // Email me (^) if my answer is selected or commented on
	'a_waiting_your_approval' => 'Tato reakce čeká na schválení',  // This answer is waiting for your approval
	'a_your_waiting_approval' => 'Vaše reakce bude zkontrolována a schválena krátce.',  // Your answer will be checked and approved shortly.
	'add_answer_button' => 'Reagovat (přidat reakci)',  // Add answer
	'add_comment_button' => 'Přidat komentář',  // Add comment
	'add_q_favorites' => 'Přidat ttento nápad do mých oblíbených položek',  // Add this question to my favorites
	'anon_name_label' => 'Vaše jméno pro zobrazení (volitelné):',  // Your name to display (optional):
	'answer_button' => 'Reakce',  // answer
	'answer_limit' => 'Příliš mnoho přijatých reakcí - zkuste to znovu za hodinu',  // Too many answers received - please try again in an hour
	'answer_must_be_approved' => 'Váš účet musí být schválen před reakcí na nápad. Čekejte prosím OR. ^1Přidat další informace^2.',  // Your account must be approved before you answer a question. Please wait or ^1add more information^2.
	'answer_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 abyste mohl/mohla reagovat na tento nápad.',  // Please ^5confirm your email address^6 to answer this question.
	'answer_must_login' => 'Prosím ^1přihláste se^2 nebo ^3registrujte^4 abyste mohl/mohla reagovat na tento nápad.',  // Please ^1log in^2 or ^3register^4 to answer this question.
	'answer_q_popup' => 'Reakce na tento nápad',  // Answer this question
	'approve_a_popup' => 'Schválit tuto reakci',  // Approve this answer
	'approve_button' => 'schvalovat',  // approve
	'approve_c_popup' => 'Schválit tento komentář',  // Approve this comment
	'approve_q_popup' => 'Schválit tento nápad',  // Approve this question
	'ask_button' => 'Zeptej se',  // Ask the Question
	'ask_follow_from_a' => 'Vaše nápad se vztahuje k této reakci:',  // Your question will be related to this answer:
	'ask_follow_title' => 'Vložte podobný nápad',  // Ask a related question
	'ask_limit' => 'Příliš mnoho obdržených nápadů - Zkuste to prosím znovu za hodinu',  // Too many questions received - please try again in an hour
	'ask_must_be_approved' => 'Váš účet musí být schválen dříve, než se zeptáte na nápad. Čekejte prosím OR. ^1Přidat další informace^2.',  // Your account must be approved before you ask a question. Please wait or ^1add more information^2.
	'ask_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 abyste se mohli zeptat se na nápad.',  // Please ^5confirm your email address^6 to ask a question.
	'ask_must_login' => 'Prosím ^1přihlásit se^2 nebo ^3Registrovat^4  abyste se mohli zeptat se na nápad.',  // Please ^1log in^2 or ^3register^4 to ask a question.
	'ask_same_q' => 'Před pokračováním, prosím, zkontrolujte, zda nebyl nápad již zadán:',  // Before proceeding, please check your question was not asked already:
	'ask_title' => 'Vložit nápad',  // Ask a question
	'c_notify_email' => 'Napište mi na této adrese, pokud je přidán komentář po dolu:',  // Email me at this address if a comment is added after mine:
	'c_notify_label' => 'Napište mi, pokud je přidán komentář',  // Email me if a comment is added after mine
	'c_notify_x_label' => 'Napiš mi email (^) Pokud je přidáno komentář',  // Email me (^) if a comment is added after mine
	'c_waiting_your_approval' => 'Tento komentář čeká na vaše schválení',  // This comment is waiting for your approval
	'c_your_waiting_approval' => 'Váš komentář bude kontrolován a schválen krátce.',  // Your comment will be checked and approved shortly.
	'category_ask_not_allowed' => 'Nemáte povolení vkládat nápady v této kategorii',  // You do not have permission to ask questions in this category
	'category_js_note' => 'Chcete-li vybrat libovolnou kategorii, povolte JavaScript ve svém webovém prohlížeči.',  // To select any category, please enable Javascript in your web browser.
	'category_required' => 'Prosím vyberte kategorii',  // Please choose a category
	'claim_a_popup' => 'Přiřaďte tuto reakci na váš uživatelský účet',  // Assign this answer to your user account
	'claim_button' => 'Napsal jsem to',  // I wrote this
	'claim_c_popup' => 'Přiřaďte tento komentář k vašemu uživatelskému účtu',  // Assign this comment to your user account
	'claim_q_popup' => 'Přiřaďte tento nápad svého uživatelského účtu',  // Assign this question to your user account
	'clear_flags_button' => 'odstranit označení',  // clear flags
	'clear_flags_popup' => 'Odstraňte označení všemi uživateli',  // Remove flags by all users
	'close_button' => 'zavřít',  // close
	'close_duplicate' => 'To je duplikát jiného nápadu',  // This is a duplicate of another question
	'close_duplicate_error' => 'Duplicitní nápad nebyl nalezen - zkuste zadat číslo z jiného nápadu (např. 123) URL nápadu.',  // The duplicate question could not be found - please try entering the number from a different question URL, e.g. 123.
	'close_form_button' => 'Uzavřít nápad',  // Close question
	'close_form_title' => 'Uzavřete tento nápad',  // Close this question
	'close_original_note' => 'Můžete také zadat číslo nápadu z adresy URL, např. 123.',  // You can also enter the question number from the URL, e.g. 123.
	'close_original_title' => 'URL původního nápadu:',  // URL of the original question:
	'close_q_popup' => 'Zavřete tento nápad na všechny nové reakce',  // Close this question to any new answers
	'close_reason_title' => 'Důvod pro uzavření tohoto nápadu nebo URL duplicitního nápadu:',  // Reason for closing this question, or URL of duplicate question:
	'closed_as_duplicate' => 'Uzavřeno jako duplikát:',  // closed as a duplicate of:
	'closed_with_note' => 'Uzavřeno s poznámkou:',  // closed with the note:
	'comment_a_popup' => 'Přidat komentář k této reakci',  // Add a comment on this answer
	'comment_button' => 'komentář',  // comment
	'comment_limit' => 'Příliš mnoho přijatých komentářů - Zkuste to prosím znovu za hodinu',  // Too many comments received - please try again in an hour
	'comment_must_be_approved' => 'Váš účet musí být schválen před přidáním komentáře. Čekejte prosím OR. ^1Přidat další informace^2.',  // Your account must be approved before you add a comment. Please wait or ^1add more information^2.
	'comment_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 přidat komentář.',  // Please ^5confirm your email address^6 to add a comment.
	'comment_must_login' => 'Prosím ^1přihlásit se^2 nebo ^3Registrovat^4 přidat komentář.',  // Please ^1log in^2 or ^3register^4 to add a comment.
	'comment_on_a' => 'Na reakce:',  // On answer:
	'comment_on_q' => 'K nápadu:',  // On question:
	'comment_q_popup' => 'Přidat komentář k tomuto nápadu',  // Add a comment on this question
	'delete_a_popup' => 'Odstranit tuto reakci trvale',  // Delete this answer permanently
	'delete_button' => 'vymazat',  // delete
	'delete_c_popup' => 'Odstranit tento příspěvek trvale',  // Delete this comment permanently
	'delete_q_popup' => 'Odstranit tento nápad trvale',  // Delete this question permanently
	'duplicate_content' => 'Zdá se, že vaše podání je duplikát.',  // Your submission appears to be a duplicate.
	'edit_a_popup' => 'Upravit tuto reakci',  // Edit this answer
	'edit_a_title' => 'Upravit reakci',  // Edit answer
	'edit_button' => 'Upravit',  // edit
	'edit_c_popup' => 'Upravit tento komentář',  // Edit this comment
	'edit_c_title' => 'Upravit komentář',  // Edit comment
	'edit_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 upravit toto.',  // Please ^5confirm your email address^6 to edit this.
	'edit_must_login' => 'Prosím ^1přihlásit se^2 nebo ^3Registrovat^4 upravit toto.',  // Please ^1log in^2 or ^3register^4 to edit this.
	'edit_q_popup' => 'Upravit tento nápad',  // Edit this question
	'edit_q_title' => 'Upravit nápad',  // Edit Question
	'example_tags' => 'Příklad tagů:',  // Example tags: 
	'flag_a_popup' => 'Označení této reakce jako spam nebo nevhodný obsah',  // Flag this answer as spam or inappropriate
	'flag_button' => 'Označení',  // flag
	'flag_c_popup' => 'Označení tohoto komentáře jako spam nebo nevhodný obsah',  // Flag this comment as spam or inappropriate
	'flag_hide_button' => 'skrýt označení',  // flag and hide
	'flag_limit' => 'Příliš mnoho příspěvků označen - zkuste to prosím znovu za hodinu',  // Too many posts flagged - please try again in an hour
	'flag_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 do příznaků.',  // Please ^5confirm your email address^6 to flag posts.
	'flag_must_login' => 'Prosím ^1přihlásit se^2 nebo ^3Registrovat^4 do příznaků.',  // Please ^1log in^2 or ^3register^4 to flag posts.
	'flag_not_allowed' => 'Označování Toto není povoleno',  // Flagging this is not allowed
	'flag_q_popup' => 'Označit tento nápad jako spam nebo nevhodný obsah',  // Flag this question as spam or inappropriate
	'follow_a_popup' => 'Zeptejte se nové nápady týkající se této reakce',  // Ask a new question relating to this answer
	'follow_button' => 'Zeptejte se příbuzné nápady',  // ask related question
	'follows_a' => 'související s reakcí pro:',  // related to an answer for:
	'follows_q' => 'O nápadu:',  // about the question:
	'hide_a_popup' => 'Skrýt tuto reakci',  // Hide this answer
	'hide_button' => 'skrýt',  // hide
	'hide_c_popup' => 'Skrýt tento komentář',  // Hide this comment
	'hide_q_popup' => 'Skrýt tento nápad',  // Hide this question
	'matching_tags' => 'Odpovídající značky:',  // Matching tags: 
	'max_tags_x' => 'Maximálně ^ Štítky jsou povoleny',  // A maximum of ^ tags are allowed
	'min_tags_x' => 'Uveďte alespoň alespoň ^ Tag / S.',  // Please provide at least ^ tag/s
	'notify_email_note' => 'Soukromí: Vaše e-mailová adresa bude použita pouze pro odesílání těchto oznámení.',  // Privacy: Your email address will only be used for sending these notifications.
	'q_category_label' => 'Kategorie:',  // Category:
	'q_content_label' => 'Více informací k nápadu:',  // More information for the question:
	'q_hidden_author' => 'Tento nápad byl skryt svým autorem',  // This question has been hidden by its author
	'q_hidden_flagged' => 'Tento nápad byl označen a skryt',  // This question has been flagged and hidden
	'q_hidden_other' => 'Tento nápad byl skryt',  // This question has been hidden
	'q_notify_email' => 'Napište mi na této adrese, pokud jsou na můj nápad nějaké reakce nebo komentáře:',  // Email me at this address if my question is answered or commented on:
	'q_notify_label' => 'Napište mi, pokud jsou reakce nebo komentáře na můj nápad',  // Email me if my question is answered or commented on
	'q_notify_x_label' => 'Napiš mi email (^) pokud jsou reakce nebo komentáře na můj nápad',  // Email me (^) if my question is answered or commented on
	'q_tags_comma_label' => 'Štítky - užijte čárku (,) jako oddělovač:',  // Tags - use comma (,) as a separator:
	'q_tags_label' => 'Štítky - Použijte pomlčky pro kombinování slov:',  // Tags - use hyphens to combine words:
	'q_title_label' => 'Nápad v jedné větě:',  // The question in one sentence:
	'q_waiting_approval' => 'Tento nápad čeká na schválení',  // This question is waiting for approval
	'q_waiting_your_approval' => 'Tento nápad čeká na vaše schválení',  // This question is waiting for your approval
	'q_your_waiting_approval' => 'Váš nápad  brzo zkontrolován a schválen.',  // Your question will be checked and approved shortly.
	'recat_button' => 'rekreategorizace',  // recategorize
	'recat_popup' => 'Změnit tuto kategorii nápadů',  // Change this question\'s category
	'recat_q_title' => 'Změnit kategorii nápadu',  // Recategorize question
	'reject_a_popup' => 'Odmítnout tuto reakci',  // Reject this answer
	'reject_button' => 'odmítnout',  // reject
	'reject_c_popup' => 'Odmítnout tento komentář',  // Reject this comment
	'reject_q_popup' => 'Odmítnout tento nápad',  // Reject this question
	'remove_q_favorites' => 'Odstranit tento nápad z mých oblíbených',  // Remove this question from my favorites
	'reopen_button' => 'znovu otevřít',  // reopen
	'reopen_q_popup' => 'Znovu otevřít tento nápad',  // Reopen this question
	'reply_button' => 'reagovat',  // reply
	'reply_c_popup' => 'Reagovat na tento komentář',  // Reply to this comment
	'reshow_a_popup' => 'Znovu zobrazit tuto reakci',  // Znovu zobrazit this answer
	'reshow_button' => 'znovu zobrazit',  // reshow
	'reshow_c_popup' => 'Znovu zobrazit tento komentář',  // Znovu zobrazit this comment
	'reshow_q_popup' => 'Znovu zobrazit tento nápad',  // Znovu zobrazit this question
	'retag_button' => 'retag',  // retag
	'retag_cat_popup' => 'Změňte tuto kategorii nebo značky',  // Change this question\'s category or tags
	'retag_popup' => 'Změňte štítky pro tento nápad',  // Change this question\'s tags
	'retag_q_title' => 'Změnit štítky nápadu.',  // Retag question
	'save_silent_label' => 'Uložit v tichosti, aby se skrylo, že byla tato položka upravena.',  // Save silently to hide that this was edited
	'select_popup' => 'Kliknutím vyberte jako nejlepší reakci',  // Click to select as best answer
	'select_text' => 'Nejlepší reakce',  // Best answer
	'show_1_comment' => 'Zobrazit 1 komentář',  // Show 1 comment
	'show_1_previous_comment' => 'Zobrazit 1 předchozí komentář',  // Show 1 previous comment
	'show_x_comments' => 'Ukázat ^ komentáře',  // Show ^ comments
	'show_x_previous_comments' => 'Ukázat ^ Předchozí komentáře',  // Show ^ previous comments
	'unflag_button' => 'neflagovaný',  // unflag
	'unflag_popup' => 'Odstraňte označení, které jste přidali',  // Remove the flag that you added
	'unselect_popup' => 'Kliknutím odstraníte výběr',  // Click to remove selection
	'x_answers_title' => '^ Reakce',  // ^ Answers
	'your_answer_title' => 'Tvoje reakce',  // Your answer
	'your_comment_a' => 'Váš komentář k této reakce:',  // Your comment on this answer:
	'your_comment_q' => 'Váš komentář k tomuto nápadu:',  // Your comment on this question:
);
