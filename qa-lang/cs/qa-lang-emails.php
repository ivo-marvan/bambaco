<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	Description: Language phrases for email notifications


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php

	Translated automatically by the software 
		"https://github.com/ivomarvan/samples_and_experiments/machine_translation_question2answer"
		2022-01-31 20:09:38.578341
		To language: cs => Czech - Čeština
*/

return array(
    	// Your answer on ^site_title has a new comment by ^c_handle:\n\n^open^c_content^close\n\nYour answer was:\n\n^open^c_context^close\n\nYou may respond by adding your own comment:\n\n^url\n\nThank you,\n\n^site_title
	'a_commented_body' => 'Reakce na ^site_title má nový komentář ^c_handle:\n\n^open^c_content.^close\n\nvaše reakce byla:\n\n^open^c_context.^close\n\nmůže reagovat přidáním vlastního komentáře:\n\n^URL^Titulek webové stránky',


	// Your ^site_title answer has a new comment
	'a_commented_subject' => 'Váš ^site_title reakce má nový komentář',


	// Your answer on ^site_title has a new related question by ^q_handle:\n\n^open^q_title^close\n\nYour answer was:\n\n^open^a_content^close\n\nClick below to answer the new question:\n\n^url\n\nThank you,\n\n^site_title
	'a_followed_body' => 'Reakce na ^site_title má nový související nápad ^q_handle:\n\n^open^Q_title.^close\n\nvaše reakce byla:\n\n^open^a_content.^close Klepněte na tlačítko níže a reagujte na nový nápad: \t^URL^Titulek webové stránky',


	// Your ^site_title answer has a related question
	'a_followed_subject' => 'Váš ^site_title reakce má související nápad',


	// Congratulations! Your answer on ^site_title has been selected as the best by ^s_handle:\n\n^open^a_content^close\n\nThe question was:\n\n^open^q_title^close\n\nClick below to see your answer:\n\n^url\n\nThank you,\n\n^site_title
	'a_selected_body' => 'Gratulujeme! Vaše reakce na ^site_title byla vybrán jako nejlepší ^s_handle:\n\n^open^a_content.^close \t^open^Q_title.^close Klikněte na níže zobrazit reakci:\n\n^URL^Titulek webové stránky',


	// Your ^site_title answer has been selected!
	'a_selected_subject' => 'Váše ^site_title reakce byla vybrána!',


	// A new comment by ^c_handle has been added after your comment on ^site_title:\n\n^open^c_content^close\n\nThe discussion is following:\n\n^open^c_context^close\n\nYou may respond by adding another comment:\n\n^url\n\nThank you,\n\n^site_title
	'c_commented_body' => 'Nový komentář ^c_handle byl přidán po vašem komentáři ^site_title:\n\n^open^c_content.^Diskuse je následující:\n\n^open^c_context.^close\n\nyou může reagovat přidáním jiného komentáře:\n\n^URL^Titulek webové stránky',  


	// Your ^site_title comment has been added to
	'c_commented_subject' => 'Váš ^site_title Komentář byl přidán',  


	// Please click below to confirm your email address for ^site_title.\n\n^url\n\nConfirmation code: ^code\n\n Thank you,\n^site_title
	'confirm_body' => 'Pro potvrzení e-mailové adresy klikněte níže ^site_title.\n\n^URL nkonfirmage kód: ^kód\n\n Děkujeme,\n^Titulek webové stránky',  


	// ^site_title - Email Address Confirmation
	'confirm_subject' => '^site_title - Potvrzení e-mailové adresy',  


	// Comments:\n^message\n\nName:\n^name\n\nEmail:\n^email\n\nPrevious page:\n^previous\n\nUser:\n^url\n\nIP address:\n^ip\n\nBrowser:\n^browser
	'feedback_body' => 'Komentáře:\n^Nesprávná zpráva:\n\n^E-mail na stránce \t^Předchozí Numer:\n^adresa adresy URL:\n^IP nBrowser:\n^prohlížeč',  


	// ^ feedback
	'feedback_subject' => '^ zpětná vazba',  


	// A post by ^p_handle has received ^flags:\n\n^open^p_context^close\n\nClick below to see the post:\n\n^url\n\n\nClick below to review all flagged posts:\n\n^a_url\n\n\nThank you,\n\n^site_title
	'flagged_body' => 'Příspěvek ^p_handle obdržel ^označení:\n\n^open^p_context.^close Klikněte na níže zobrazit příspěvek:\n\n^URL^a_url^Titulek webové stránky',


	// ^site_title has a flagged post
	'flagged_subject' => '^site_title má příznakový post',  


	// A post by ^p_handle requires your approval:\n\n^open^p_context^close\n\nClick below to approve or reject the post:\n\n^url\n\n\nClick below to review all queued posts:\n\n^a_url\n\n\nThank you,\n\n^site_title
	'moderate_body' => 'Příspěvek ^p_handle vyžaduje schválení:\n\n^open^p_context.^close NNClick níže schválit nebo odmítnout příspěvek:\n\n^Namontujte adresu URL^a_url^Titulek webové stránky',  


	// ^site_title moderation
	'moderate_subject' => '^site_title moderation.',  


	// Your new password for ^site_title is below.\n\nPassword: ^password\n\nIt is recommended to change this password immediately after logging in.\n\nThank you,\n^site_title\n^url
	'new_password_body' => 'Vaše nové heslo pro ^site_title je níže. NPPASSWORD: ^heslo NIT se doporučuje změnit toto heslo ihned po přihlášení.^site_title \t^url',  


	// ^site_title - Your New Password
	'new_password_subject' => '^site_title - Vaše nové heslo',  


	// You have been sent a private message by ^f_handle on ^site_title:\n\n^open^message^close\n\n^moreThank you,\n\n^site_title\n\n\nTo block private messages, visit your account page:\n^a_url
	'private_message_body' => 'Byl jste odeslán soukromé zprávy ^f_handle on. ^site_title:\n\n^open^zpráva^zavřít\n\n^Morethance vás,\n\n^site_title\n\n NO blokovat soukromé zprávy, navštivte stránku účtu:^a_url.',  


	// More information about ^f_handle:\n\n^url\n\n
	'private_message_info' => 'Více informací ^f_handle:\n\n^URL\n\n',  


	// Click below to reply to ^f_handle by private message:\n\n^url\n\n
	'private_message_reply' => 'Klikněte níže a reagujte ^f_handle na soukromou zprávu:\n\n^URL\n\n',


	// Message from ^f_handle on ^site_title
	'private_message_subject' => 'Zpráva od ^f_handle on. ^Titulek webové stránky',  


	// Your question on ^site_title has been answered by ^a_handle:\n\n^open^a_content^close\n\nYour question was:\n\n^open^q_title^close\n\nIf you like this answer, you may select it as the best:\n\n^url\n\nThank you,\n\n^site_title
	'q_answered_body' => 'K vašemu nápadu ^site_title byla zaslána reakce ^a_handle:\n\n^open^a_content.^close\n\nváš nápad byl:\n\n^otevřen^Q_title.^close\n\nJestli s vám líbí tato reakce, můžete ji vybrat jako nejlepší:\n\n^URL^Titulek webové stránky',


	// Your ^site_title question was answered
	'q_answered_subject' => 'Na váš nápad ^site_title přišla reakce',


	// Your question on ^site_title has a new comment by ^c_handle:\n\n^open^c_content^close\n\nYour question was:\n\n^open^c_context^close\n\nYou may respond by adding your own comment:\n\n^url\n\nThank you,\n\n^site_title
	'q_commented_body' => 'Vaše nápad ^site_title má nový komentář ^c_handle:\n\n^open^c_content.^close\n\nváš nápad byl:\n\n^open^c_context.^close\n\nyou může reagovat přidáním vlastního komentáře:\n\n^URL^Titulek webové stránky',


	// Your ^site_title question has a new comment
	'q_commented_subject' => 'Váš ^site_title nápad má nový komentář',


	// A new question has been asked by ^q_handle:\n\n^open^q_title\n\n^q_content^close\n\nClick below to see the question:\n\n^url\n\nThank you,\n\n^site_title
	'q_posted_body' => 'Byl vložen nový nápad ^q_handle:\n\n^open^q_title\n\n^q_content.^Chcete-li zobrazit nápad, klepněte na tlačítko níže: \t^URL^Titulek webové stránky',


	// ^site_title has a new question
	'q_posted_subject' => '^site_title má nový nápad',


	// An edited post by ^p_handle requires your reapproval:\n\n^open^p_context^close\n\nClick below to approve or hide the edited post:\n\n^url\n\n\nClick below to review all queued posts:\n\n^a_url\n\n\nThank you,\n\n^site_title
	'remoderate_body' => 'Upravený příspěvek ^p_handle vyžaduje váš reapproval:\n\n^open^p_context.^Uzavřete NNCLICK níže Chcete-li schválit nebo skrýt upravený příspěvek:\n\n^Namontujte adresu URL^a_url^Titulek webové stránky',  


	// ^site_title moderation
	'remoderate_subject' => '^site_title moderation.',  


	// Please click below to reset your password for ^site_title.\n\n^url\n\nAlternatively, enter the code below into the field provided.\n\nCode: ^code\n\nIf you did not ask to reset your password, please ignore this message.\n\nThank you,\n^site_title
	'reset_body' => 'Chcete-li heslo obnovit, klikněte prosím níže ^site_title.\n\n^URL Nalternativně zadejte kód níže do uvedeného pole. ^Pokud jste nepožádali o obnovení hesla, prosím, ignorujte tuto zprávu. \t^Titulek webové stránky',  


	// ^site_title - Reset Forgotten Password
	'reset_subject' => '^site_title - reset zapomenutého hesla',  


	// ^,\n\n
	'to_handle_prefix' => '^,\n\n',  


	// A new user has registered as ^u_handle.\n\nClick below to view the user profile:\n\n^url\n\nThank you,\n\n^site_title
	'u_registered_body' => 'Nový uživatel se zaregistroval jako ^U_handle. Níže uvedený uživatelský profil:\n\n^URL^Titulek webové stránky',  


	// ^site_title has a new registered user
	'u_registered_subject' => '^site_title má nového registrovaného uživatele',  


	// A new user has registered as ^u_handle.\n\nClick below to approve the user:\n\n^url\n\nClick below to review all users waiting for approval:\n\n^a_url\n\nThank you,\n\n^site_title
	'u_to_approve_body' => 'Nový uživatel se zaregistroval jako ^u_handle.^URL^a_url^Titulek webové stránky',  


	// You can see your new user profile here:\n\n^url\n\nThank you,\n\n^site_title
	'u_approved_body' => 'Můžete vidět svůj nový profil uživatele zde:\n\n^URL^Titulek webové stránky',  


	// Your ^site_title user has been approved
	'u_approved_subject' => 'Váš ^site_title Uživatel byl schválen',  


	// ^f_handle has posted on your user wall at ^site_title:\n\n^open^post^close\n\nYou may respond to the post here:\n\n^url\n\nThank you,\n\n^site_title
	'wall_post_body' => '^f_handle zveřejnil na své uživatelské zdi na ^site_title:\n\n^open^pošta^close^URL^Titulek webové stránky',  


	// Post on your ^site_title wall
	'wall_post_subject' => 'Post na vašem ^site_title Wall.',  


	// Thank you for registering for ^site_title.\n\n^custom^confirmYour login details are as follows:\n\nUsername: ^handle\nEmail: ^email\n\nPlease keep this information safe for future reference.\n\nThank you,\n\n^site_title\n^url
	'welcome_body' => 'Děkujeme za registraci ^site_title.\n\n^Zvyk^Potvrďte přihlašovací údaje: \t ^E-mail: ^E-mail nPladease Tyto informace uchovávejte v bezpečí pro budoucí použití. \ n N4H)^site_title \t^url',  


	// Please click below to confirm your email address.\n\n^url\n\n
	'welcome_confirm' => 'Pro potvrzení e-mailové adresy klikněte níže.^URL\n\n',  


	// Welcome to ^site_title!
	'welcome_subject' => 'Vítejte v ^Titulek webové stránky!',  
);
