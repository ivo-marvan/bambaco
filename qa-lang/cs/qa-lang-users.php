<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	Description: Language phrases for user management


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php

	Translated automatically by the software 
		"https://github.com/ivomarvan/samples_and_experiments/machine_translation_question2answer"
		2022-01-31 20:38:21.083849
		To language: cs => Czech - Čeština
*/

return array(
	'about' => 'O',  // About
	'add_user_x_favorites' => 'Přidat uživatele ^ do mých oblíbených',  // Add user ^ to my favorites
	'approve_required' => 'Čekejte prosím, až bude váš účet schválen nebo ^1Přidat další informace^2.',  // Please wait for your account to be approved or ^1add more information^2.	|	 @deprecated
	'approve_title' => 'Schválení uživatele čekající',  // User approval pending
	'approve_user_button' => 'Schválit uživatele',  // Approve User
	'approved_user' => 'Schválený uživatel',  // Approved user
	'avatar_default' => 'Výchozí hodnota',  // Default
	'avatar_gravatar' => 'Ukázat můj ^1Gravatar.^2',  // Show my ^1Gravatar^2
	'avatar_label' => 'Avatar:',  // Avatar:
	'avatar_none' => 'Žádný',  // None
	'block_user_button' => 'Zablokovat uživatele',  // Block User
	'blocked_users' => 'Blokované uživatele',  // Blocked users
	'category_level_add' => '- ^1Přidejte oprávnění specifické pro kategorii^2',  //  - ^1add category-specific privileges^2
	'category_level_in' => 'Pro níže uvedenou kategorii:',  // for the category below:
	'category_level_label' => 'Aktualizujte na adresu:',  // Upgraded to:
	'category_level_none' => 'Žádný upgrade',  // No upgrade
	'change_email_link' => '- ^1změnit e-mail^2',  //  - ^1change email^2
	'change_password' => 'Změnit heslo',  // Change Password
	'confirm_button' => 'Potvrďte e-mailovou adresu',  // Confirm email address
	'confirm_complete' => 'Děkujeme - Vaše e-mailová adresa byla potvrzena.',  // Thank you - your email address has been confirmed.
	'confirm_emailed' => 'Potvrzovací odkaz byl zaslán e-mailem. Kliknutím na odkaz potvrďte svou e-mailovou adresu nebo zadejte potvrzovací kód níže.',  // A confirmation link has been emailed to you. Please click the link to confirm your email address or input the confirmation code below.
	'confirm_required' => 'Pro dokončení registrace klikněte prosím na potvrzovací odkaz, který vám bylo zasláno e-mailem nebo ^1požádat jiný^2.',  // To complete your registration, please click the confirmation link that has been emailed to you, or ^1request another^2.
	'confirm_title' => 'Potvrzení e-mailové adresy',  // Email Address Confirmation
	'confirm_wrong_log_in' => 'Kód není správný - prosím ^1přihlásit se^2 Poslat nový odkaz',  // Code not correct - please ^1log in^2 to send a new link
	'confirm_wrong_resend' => 'Kód není správný - klikněte prosím na následující odkaz',  // Code not correct - please click below to send a new link
	'delete_user_button' => 'Smazat uživatele',  // Delete User
	'edit_profile' => 'Upravit můj profil',  // Edit my Profile
	'edit_user_button' => 'Upravit uživatele.',  // Edit User
	'email_code_another' => 'poslat další',  // send another
	'email_code_emailed' => 'Byl jste e-mailem e-mailem',  // You have been emailed your code
	'email_code_label' => 'Kód:',  // Code:
	'email_code_wrong' => 'Kód není správný',  // Code not correct
	'email_confirmed' => 'Potvrzený',  // Confirmed
	'email_exists' => 'E-mail již patří k účtu',  // Email already belongs to an account
	'email_handle_label' => 'E-mail nebo uživatelské jméno:',  // Email or Username:
	'email_invalid' => 'E-mail je neplatný - zkontrolujte pozorně',  // Email is invalid - please check carefully
	'email_label' => 'E-mailem:',  // Email:
	'email_not_confirmed' => 'Ještě není potvrzeno',  // Not yet confirmed
	'email_please_confirm' => 'Prosím ^5potvrdit^6',  // Please ^5confirm^6
	'email_required' => 'Požadovaná e-mailová adresa - není veřejná',  // Email address required - not public
	'forgot_link' => 'Zapomněl jsem své heslo',  // I forgot my password
	'full_name' => 'Celé jméno',  // Full name
	'handle_blocked' => 'Uživatelské jméno je zakázáno - zkuste jiný',  // Username is disallowed - please try another
	'handle_empty' => 'Uživatelské jméno nesmí být prázdné',  // Username must not be empty
	'handle_exists' => 'Uživatelské jméno je pořízeno - zkuste jiný',  // Username is taken - please try another
	'handle_has_bad' => 'Uživatelské jméno nemusí obsahovat: ^',  // Username may not contain: ^
	'handle_label' => 'Uživatelské jméno:',  // Username:
	'hide_all_user_button' => 'Skrýt všechny příspěvky tohoto uživatele',  // Hide all posts by this user
	'last_login_label' => 'Poslední přihlášení:',  // Last login:
	'last_write_label' => 'Poslední zápis akce:',  // Last write action:
	'level_admin' => 'Správce',  // Administrator
	'level_editor' => 'Editor',  // Editor
	'level_expert' => 'Expert',  // Expert
	'level_for_category' => '^1 pro ^2',  // ^1 for ^2
	'level_in_general' => 'obecně',  // in general
	'level_moderator' => 'Moderátor',  // Moderator
	'level_super' => 'Super Administrator.',  // Super Administrator
	'location' => 'Umístění',  // Location
	'log_in_to_access' => 'Teď můžeš ^1přihlásit se^2 přístup k účtu.',  // You may now ^1log in^2 to access your account.
	'login_button' => 'Přihlásit se',  // Log In
	'login_limit' => 'Příliš mnoho pokusů o přihlášení - zkuste to znovu za hodinu',  // Too many login attempts - please try again in an hour
	'login_title' => 'Přihlásit se',  // Log in
	'mass_mailings' => 'Mass Mailings:',  // Mass mailings:
	'mass_mailings_explanation' => 'Přihlásit se k odběru e-mailů odeslaných všem uživatelům',  // Subscribe to emails sent out to all users
	'member_for' => 'Člen pro:',  // Member for:
	'member_type' => 'Typ:',  // Type:
	'new_password_1' => 'Nové heslo:',  // New password:
	'new_password_2' => 'Znovu zadejte nové heslo:',  // Retype new password:
	'no_blocked_users' => 'Nebyli nalezeni žádné blokované uživatele',  // No blocked users found
	'no_permission' => 'Nemáte povolení k provedení této operace',  // You do not have permission to perform this operation
	'old_password' => 'Staré heslo:',  // Old password:
	'only_shown_admins' => '(Zobrazeno pouze pro vás a administrátory)',  // (only shown to you and admins)
	'only_shown_editors' => '(Zobrazeno pouze pro vás, editory a výše)',  // (only shown to you, editors and above)
	'only_shown_experts' => '(Zobrazeno pouze pro vás, odborníci a výše)',  // (only shown to you, experts and above)
	'only_shown_moderators' => '(Zobrazeno pouze pro vás, moderátoři a administrátory)',  // (only shown to you, moderators and admins)
	'password_changed' => 'heslo změněno',  // Password changed
	'password_label' => 'Heslo:',  // Password:
	'password_min' => 'Heslo musí být přinejmenším ^ znaky',  // Password must be at least ^ characters
	'password_mismatch' => 'Nová hesla si neodpovídají',  // New passwords do not match
	'password_none' => 'Žádný. Chcete-li se přihlásit přímo, nastavte heslo níže.',  // None. To log in directly, set a password below.
	'password_sent' => 'Vaše nové heslo vám bylo zasláno e-mailem',  // Your new password was emailed to you
	'password_to_set' => 'Nastavte prosím na stránce účtu',  // Please set on your account page
	'password_wrong' => 'Heslo není správné',  // Password not correct
	'private_messages' => 'Soukromé zprávy:',  // Private messages:
	'private_messages_explanation' => 'Povolit uživatelům poslat e-mailem (bez zobrazení adresy)',  // Allow users to email you (without seeing your address)
	'profile_saved' => 'Uložený profil',  // Profile saved
	'register_button' => 'Registrovat',  // Register
	'register_limit' => 'Příliš mnoho registrací - zkuste to prosím znovu za hodinu',  // Too many registrations - please try again in an hour
	'register_suspended' => 'Registrace nových uživatelů byla dočasně pozastavena. Zkuste to prosím znovu.',  // Registration of new users has been temporarily suspended. Please try again soon.
	'register_title' => 'Zaregistrujte se jako nový uživatel',  // Register as a new user
	'registered_label' => 'Registrovaný:',  // Registered:
	'registered_user' => 'Registrovaný Uživatel',  // Registered user
	'remember' => 'Pamatovat',  // Remember
	'remember_label' => 'zapamatovat si mě na tomto počítači',  // Remember me on this computer
	'remove_avatar' => 'Odstranit avatar:',  // Remove avatar:
	'reset_title' => 'Resetovat zapomenuté heslo',  // Reset Forgotten Password
	'save_profile' => 'Uložit profil',  // Save Profile
	'save_user' => 'Uložit uživatele.',  // Save User
	'send_confirm_button' => 'Odeslat potvrzovací odkaz',  // Send Confirmation Link
	'send_reset_button' => 'Odeslat e-mail resetovat heslo',  // Send Reset Password Email
	'send_reset_note' => 'Zpráva bude zaslána na vaši e-mailovou adresu s pokyny.',  // A message will be sent to your email address with instructions.
	'special_users' => 'Speciální uživatelé',  // Special users
	'terms_not_accepted' => 'Musíte to zaškrtnout.',  // You must tick this.
	'unblock_user_button' => 'Odblokovat uživatele',  // Unblock User
	'unsubscribe' => 'Odhlásit odběr:',  // Unsubscribe:
	'unsubscribe_complete' => 'Byl jste odhlášen z masových zásilek odeslaných ^0. Můžete se znovu odebírat prostřednictvím svého ^1účet^2 strana.',  // You have been unsubscribed from mass mailings sent out by ^0. You may resubscribe at any time via your ^1account^2 page.
	'unsubscribe_title' => 'Odhlásit odběr',  // Unsubscribe
	'unsubscribe_wrong_log_in' => 'Kód není správný - prosím ^1přihlásit se^2 odhlásit se',  // Code not correct - please ^1log in^2 to unsubscribe
	'user_blocked' => '(blokováno)',  // (blocked)
	'user_not_found' => 'Uživatel nenalezen',  // User not found
	'wall_posts' => 'Příspěvky na zdi:',  // Wall posts:
	'wall_posts_explanation' => 'Povolit uživatelům poslat na zeď (budete také e-mailem)',  // Allow users to post on your wall (you will also be emailed)
	'website' => 'webová stránka',  // Website
	'x_ago_from_y' => '^1 z ^2',  // ^1 ago from ^2
);
