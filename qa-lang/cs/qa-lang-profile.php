<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	Description: Language phrases for user profile page


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php

	Translated automatically by the software 
		"https://github.com/ivomarvan/samples_and_experiments/machine_translation_question2answer"
		2022-01-31 20:29:38.256052
		To language: cs => Czech - Čeština
*/

return array(
	'1_chosen_as_best' => '(1 nejlepší)',  //  (1 chosen as best)
	'1_down_vote' => '1 dolů',  // 1 down vote
	'1_up_vote' => '1 up hlasování',  // 1 up vote
	'1_with_best_chosen' => '(1 s nejlepším výběrem)',  //  (1 with best answer chosen)
	'activity_by_x' => 'Činnost. \ T ^',  // Activity by ^
	'answers' => 'Reakce:',  // Answers:
	'answers_by_x' => 'Reakce podle. \ T ^',  // Answers by ^
	'bonus_points' => 'Bonusové body:',  // Bonus points:
	'comments' => 'Komentáře:',  // Comments:
	'delete_pm_popup' => 'Smazat tuto soukromou zprávu',  // Delete this private message
	'delete_wall_post_popup' => 'Smazat tuto sladu',  // Delete this wall post
	'extra_privileges' => 'Extra oprávnění:',  // Extra privileges:
	'gave_out' => 'Rozdal:',  // Gave out:
	'my_account_title' => 'Můj účet',  // My account
	'no_answers_by_x' => 'Žádné reakce ^',  // No answers by ^
	'no_posts_by_x' => 'Žádné příspěvky ^',  // No posts by ^
	'no_questions_by_x' => 'Žádné nápady ^',  // No questions by ^
	'permit_anon_view_ips' => 'Prohlížení IPS anonymních příspěvků',  // Viewing IPs of anonymous posts
	'permit_close_q' => 'Uzavření jakéhokoliv nápadu',  // Closing any question
	'permit_delete_hidden' => 'Smazání skrytých příspěvků',  // Deleting hidden posts
	'permit_edit_a' => 'Úprava jakékoli reakce',  // Editing any answer
	'permit_edit_c' => 'Úprava jakékoli komentáře',  // Editing any comment
	'permit_edit_q' => 'Úprava jakéhokoliv nápadu',  // Editing any question
	'permit_edit_silent' => 'Úpravy příspěvků tiše',  // Editing posts silently
	'permit_flag' => 'Označování příspěvků',  // Flagging posts
	'permit_hide_show' => 'Skrývá nebo zobrazující jakýkoliv příspěvek',  // Hiding or showing any post
	'permit_moderate' => 'Schválení nebo odmítnutí příspěvků',  // Approving or rejecting posts
	'permit_post_a' => 'Reakce na nápad',  // Answering questions
	'permit_post_c' => 'Přidání komentářů',  // Adding comments
	'permit_post_q' => 'Vložení nápadu',  // Asking questions
	'permit_post_wall' => 'Příspěvky na zdi uživatelů',  // Posting on user walls
	'permit_recat' => 'Změna kategorie jakéhokoliv nápadu',  // Recategorizing any question
	'permit_retag' => 'Změna štítků jakéhokoliv nápadu',  // Retagging any question
	'permit_select_a' => 'Výběr reakce na nějaký nápad',  // Selecting answer for any question
	'permit_view_q_page' => 'Prohlížení stránek nápadů',  // Viewing question pages
	'permit_view_new_users_page' => 'Zobrazení nejnovější stránky uživatelů',  // Viewing the newest users page
	'permit_view_special_users_page' => 'Zobrazení stránek speciálních uživatelů',  // Viewing the special users page
	'permit_view_voters_flaggers' => 'Prohlížení, kdo hlasoval nebo označené příspěvky',  // Viewing who voted or flagged posts
	'permit_vote_a' => 'Hlasování reakcí',  // Voting on answers
	'permit_vote_c' => 'Hlasování o komentářech',  // Voting on comments
	'permit_vote_down' => 'Hlasovací sloupky dolů',  // Voting posts down
	'permit_vote_q' => 'Hlasování o nápadec',  // Voting on questions
	'post_wall_blocked' => 'Tento uživatel nepovolil nové příspěvky na zdi',  // This user has disallowed new posts on their wall
	'post_wall_button' => 'Přidat Wall Post.',  // Add wall post
	'post_wall_empty' => 'Zadejte něco na tuto zeď',  // Please enter something to post on this wall
	'post_wall_limit' => 'Tuto hodinu nemůžete napsat více sloupků zeď',  // You cannot write more wall posts this hour
	'post_wall_must_be_approved' => 'Váš účet musí být schválen k odeslání na této zdi. Čekejte prosím OR. ^1Přidat další informace^2.',  // Your account must be approved to post on this wall. Please wait or ^1add more information^2.
	'post_wall_must_confirm' => 'Prosím ^5Potvrďte svou emailovou adresu^6 na tuto zeď.',  // Please ^5confirm your email address^6 to post on this wall.
	'post_wall_must_login' => 'Prosím ^1přihlásit se^2 nebo ^3Registrovat^4 na tuto zeď.',  // Please ^1log in^2 or ^3register^4 to post on this wall.
	'questions' => 'Nápady:',  // Questions:
	'questions_by_x' => 'Nápady. \ T ^',  // Questions by ^
	'ranked_x' => '(hodnoceno #^)',  //  (ranked #^)
	'received' => 'Přijaté:',  // Received:
	'recent_activity_by_x' => 'Nedávná činnost. \ T ^',  // Recent activity by ^
	'score' => 'Skóre:',  // Score:
	'send_private_message' => '- ^1Odeslat soukromou zprávu^2',  //  - ^1send private message^2
	'set_bonus_button' => 'Aktualizovat bonus',  // Update bonus
	'title' => 'Titul:',  // Title:
	'user_x' => 'Uživatel ^',  // User ^
	'user_x_disabled_pms' => 'Uživatel ^ zakázal soukromé zprávy.',  // User ^ has disabled private messages.
	'voted_on' => 'Hlasoval:',  // Voted on:
	'wall_for_x' => 'Zdi ^',  // Wall for ^
	'wall_view_more' => 'Zobrazit více stěnových příspěvků ...',  // View more wall posts...
	'x_chosen_as_best' => '(^ zvolené co nejlépe)',  //  (^ chosen as best)
	'x_down_votes' => '^ Dolů hlasů',  // ^ down votes
	'x_up_votes' => '^ hlasování',  // ^ up votes
	'x_with_best_chosen' => '(^ S vybranou nejlepší reakcí)',  //  (^ with best answer chosen)
);
