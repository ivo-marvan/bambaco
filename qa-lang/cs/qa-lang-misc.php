<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	Description: Miscellaneous language phrases


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php

	Translated automatically by the software 
		"https://github.com/ivomarvan/samples_and_experiments/machine_translation_question2answer"
		2022-01-31 20:19:10.505550
		To language: cs => Czech - Čeština
*/

return array(
	'block_ip_button' => 'Bloková adresa IP',  // Block IP address
	'browse_categories' => 'Procházet kategorie',  // Browse categories
	'captcha_approve_fix' => 'Toto ověření se zastaví, jakmile je váš účet schválen.',  // This verification will stop appearing once your account is approved.
	'captcha_confirm_fix' => 'Chcete-li se vyhnout tomuto ověření v budoucnu, prosím ^5Potvrďte svou emailovou adresu^6.',  // To avoid this verification in future, please ^5confirm your email address^6.
	'captcha_error' => 'Vyplňte prosím ověření proti spamu',  // Please complete the anti-spam verification
	'captcha_label' => 'Ověření proti spamu:',  // Anti-spam verification:
	'captcha_login_fix' => 'Chcete-li se vyhnout tomuto ověření v budoucnu, prosím ^1přihlásit se^2 nebo ^3Registrovat^4.',  // To avoid this verification in future, please ^1log in^2 or ^3register^4.
	'feed_a_edited_prefix' => 'Reakce upravená:',  // Answer edited:
	'feed_a_prefix' => 'Reagováno:',  // Answered:
	'feed_a_reshown_prefix' => 'Raakce znovu zobrazena:',  // Answer reshown:
	'feed_a_selected_prefix' => 'Vybraná reakce:',  // Answer selected:
	'feed_c_edited_prefix' => 'Komentář upraven:',  // Comment edited:
	'feed_c_moved_prefix' => 'Komentář se přesune:',  // Comment moved: 
	'feed_c_prefix' => 'Komentoval:',  // Commented: 
	'feed_c_reshown_prefix' => 'Komentář znovu zobrazen:',  // Comment reshown:
	'feed_closed_prefix' => 'Zavřeno:',  // Closed: 
	'feed_edited_prefix' => 'Upraveno:',  // Edited: 
	'feed_hidden_prefix' => 'Skrytý:',  // Hidden: 
	'feed_not_found' => 'Feed nebyl nalezen',  // Feed not found
	'feed_recategorized_prefix' => 'Recategize:',  // Recategorized: 
	'feed_reopened_prefix' => 'Znovuotevření:',  // Reopened: 
	'feed_reshown_prefix' => 'Reshown:',  // Reshown: 
	'feed_retagged_prefix' => 'Retagged:',  // Retagged: 
	'feedback_email' => 'Váš e-mail: (volitelné)',  // Your email: (optional)
	'feedback_empty' => 'Použijte prosím toto pole pro odeslání některých komentářů nebo návrhů',  // Please use this field to send some comments or suggestions
	'feedback_message' => 'Vaše komentáře nebo návrhy ^:',  // Your comments or suggestions for ^:
	'feedback_name' => 'Vaše jméno: (volitelné)',  // Your name: (optional)
	'feedback_sent' => 'Vaše zpráva níže byla odeslána - Děkuji.',  // Your message below was sent - thank you.
	'feedback_title' => 'Poslat zpětnou vazbu',  // Send feedback
	'form_security_again' => 'Potvrďte prosím znovu',  // Please click again to confirm
	'form_security_reload' => 'Znovu načtěte stránku a zkuste to znovu',  // Please reload the page then try again
	'hide_all_ip_button' => 'Skrýt všechny příspěvky z této IP',  // Hide all posts from this IP
	'host_name' => 'Jméno hostitele:',  // Host name:
	'inbox' => 'Doručená pošta',  // Inbox
	'matches_blocked_ips' => 'Zápasy blokovaných adres IP:',  // Matches blocked IP addresses:
	'message_empty' => 'Zadejte prosím svou zprávu, kterou chcete poslat tomuto uživateli',  // Please enter your message to send to this user
	'message_explanation' => 'To bude odesláno jako oznámení ^. Vaše e-mailová adresa nebude odhalena, pokud jej nezahrnujete do zprávy.',  // This will be sent as a notification from ^. Your email address will not be revealed unless you include it in the message.
	'message_for_x' => 'Vaše zpráva ^:',  // Your message for ^:
	'message_limit' => 'Tuto hodinu nelze odeslat více soukromých zpráv',  // You cannot send more private messages this hour
	'message_must_login' => 'Prosím ^1přihlásit se^2 nebo ^3Registrovat^4 posílat soukromé zprávy.',  // Please ^1log in^2 or ^3register^4 to send private messages.
	'message_recent_history' => 'Nedávná korespondence ^',  // Recent correspondence with ^
	'message_sent' => 'Vaše soukromá zpráva níže byla odeslána',  // Your private message below was sent
	'more_favorite_qs' => 'Další oblíbené nápady ...',  // More favorite questions...
	'more_favorite_tags' => 'Více oblíbených značek ...',  // More favorite tags...
	'more_favorite_users' => 'Oblíbení více uživatelů ...',  // More favorite users...
	'my_favorites_title' => 'Můj oblíbený',  // My favorites
	'nav_all_my_updates' => 'Všechny mé aktualizace',  // All my updates
	'nav_my_content' => 'Můj obsah',  // My content
	'nav_my_details' => 'Můj účet',  // My account
	'nav_my_favorites' => 'Můj oblíbený',  // My favorites
	'nav_user_activity' => 'Poslední aktivita',  // Recent activity
	'nav_user_as' => 'Všechny reakce',  // All answers
	'nav_user_pms' => 'Soukromé zprávy',  // Private messages
	'nav_user_qs' => 'Všechny nápady',  // All questions
	'nav_user_wall' => 'stěna',  // Wall
	'next_step' => 'Další krok',  // Next step
	'no_activity_from_x' => 'Žádná činnost ^',  // No activity from ^
	'no_favorite_categories' => 'Žádné oblíbené kategorie',  // No favorite categories
	'no_favorite_qs' => 'Žádné oblíbené nápady',  // No favorite questions
	'no_favorite_tags' => 'Žádné oblíbené značky',  // No favorite tags
	'no_favorite_users' => 'Žádné oblíbené uživatele',  // No favorite users
	'no_recent_updates' => 'Žádné nedávné aktualizace',  // No recent updates
	'no_updates_content' => 'Žádné nedávné aktualizace pro můj obsah',  // No recent updates for my content
	'no_updates_favorites' => 'Žádné aktualizace pro mé oblíbené',  // No updates for my favorites
	'outbox' => 'Odeslané položky',  // Sent items
	'pm_inbox_title' => 'Soukromé zprávy obdržené',  // Private messages received
	'pm_outbox_title' => 'Soukromé zprávy odeslané',  // Private messages sent
	'private_message_title' => 'Odeslat soukromou zprávu',  // Send a private message
	'recent_activity_from_x' => 'Nedávná aktivita ^',  // Recent activity from ^
	'recent_updates_content' => 'Nedávné aktualizace pro můj obsah',  // Recent updates for my content
	'recent_updates_favorites' => 'Nedávné aktualizace pro mé oblíbené položky',  // Recent updates for my favorites
	'recent_updates_title' => 'Nedávné aktualizace pro mě',  // Recent updates for me
	'site_in_maintenance' => 'Tato stránka je v současné době dole pro údržbu - vraťte se brzy.',  // This site is currently down for maintenance - please come back soon.
	'suggest_favorites_add' => 'Chcete-li přidat nápad nebo jinou položku do oblíbených, klepněte na tlačítko ^ v horní části stránky.',  // To add a question or other item to your favorites, click the ^ at the top of its page.
	'suggest_update_favorites' => 'Pro více aktualizací přidejte položky do ^1Vaše oblíbené^2.',  // For more updates, add items to ^1your favorites^2.
	'unblock_ip_button' => 'Odblokovat IP adresu',  // Unblock IP address
	'your_a_commented' => 'vaše reakce byla komentována',  // your answer commented
	'your_a_edited' => 'vaše reakce byla upravena',  // your answer edited
	'your_a_hidden' => 'vaše reakce byla skryt',  // your answer hidden
	'your_a_questioned' => 'zeptal se na vaši reakce',  // asked on your answer
	'your_a_reshown' => 'vaše reakce byla znovu zobrazena.',  // your answer reshown
	'your_a_selected' => 'vaše reakce byla vybrána',  // your answer selected
	'your_c_edited' => 'váš komentář byl upraven',  // your comment edited
	'your_c_followed' => 'následoval váš komentář',  // your comment followed
	'your_c_hidden' => 'váš komentář je skrytý',  // your comment hidden
	'your_c_moved' => 'váš komentář se přesunul',  // your comment moved
	'your_c_reshown' => 'váš komentář byl znovu zobrazen.',  // your comment reshown
	'your_q_answered' => 'Někdo reagoval na váš nápad',  // your question answered
	'your_q_closed' => 'Vaš nápad se uzavřená',  // your question closed
	'your_q_commented' => 'Někdo komentoval váš nápad',  // your question commented
	'your_q_edited' => 'Váš nápad byl upraven',  // your question edited
	'your_q_hidden' => 'Váš nápad byl skryt',  // your question hidden
	'your_q_recategorized' => 'Byla změněna kategorie u vašeho nápadu',  // your question recategorized
	'your_q_reopened' => 'Vaše nápad byl znovu otevřen',  // your question reopened
	'your_q_reshown' => 'Váš nápad byl znovu zobrazen.',  // your question reshown
	'your_q_retagged' => 'Byly změněny štítky u vašeho nápadu',  // your question retagged
);
